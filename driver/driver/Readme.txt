smartwatch - SmartWatch Presenter driver

This driver handles the input from the SmartWatch Presenter.

##### Compilation #####

Run

  make
to compile the SmartWatch Presenter driver.

You may run

  sudo make install
to copy the compiled driver to the appropriate system directory and run

  sudo depmod
to update the module dependencies.


##### Usage #####

Run

  sudo insmod smartwatch.ko
to load the driver.

Run

  sudo rmmod smartwatch.ko
to unload the driver.

If you have installed the driver and updated the module dependencies you may also use

  sudo modprobe smartwatch
to load the driver and

  sudo modprobe -r smartwatch
to unload the driver.

##### Developer documentation #####

The SmartWatch Presenter driver registers to be used to handle devices with type SERIO_RS232 and protocol 0x40. 0x40 is not used in the official kernel at the moment. The id and the extra parameters are ignored.

When a serial port is connected to the SmartWatch Presenter driver a device with a mouse wheel is registered to the input subsystem.

Input data are received byte-wise. If 0x01 or 0x02 is received, an appropriate relative movement of the mouse wheel is reported to the input subsystem. If any other value is received a warning message is printed to the kernel log.
