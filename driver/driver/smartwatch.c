/*
 *  Author: TUM - LinuxC Lab - SmartWatch Group 2
 *
 *  based on serial mouse driver by Vojtech Pavlik
 */

/*
 *  SmartWatch Presenter driver for Linux
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/serio.h>

#define DRIVER_DESC	"SmartWatch Presenter driver"

MODULE_AUTHOR("TUM - LinuxC Lab - SmartWatch Group 2");
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");

struct smartwatch {
  struct input_dev *dev;
  signed char buf[8];
  unsigned char count;
  unsigned char type;
  unsigned long last;
  char phys[32];
};

/*
 * smartwatch_interrupt() handles incoming characters
 */

static irqreturn_t smartwatch_interrupt(struct serio *serio,
				      unsigned char data, unsigned int flags)
{
  struct input_dev *dev = ((struct smartwatch *) serio_get_drvdata(serio))->dev;

  if(data == 0x01) 
  {
    input_report_rel(dev, REL_WHEEL,  1);
  } 
  else if(data == 0x02)
  {
    input_report_rel(dev, REL_WHEEL,  -1);
  } 
  else
  {
    printk(KERN_WARNING
	   "smartwatch.c: ignoring input byte (%X)\n", data);
  }

  input_sync(dev);

  return IRQ_HANDLED;
}

/*
 * smartwatch_disconnect() cleans up after we don't want talk
 * to the mouse anymore.
 */

static void smartwatch_disconnect(struct serio *serio)
{
  struct smartwatch *smartwatch = serio_get_drvdata(serio);

  serio_close(serio);
  serio_set_drvdata(serio, NULL);
  input_unregister_device(smartwatch->dev);
  kfree(smartwatch);
}

/*
 * smartwatch_connect() is a callback form the serio module when
 * an unhandled serio port is found.
 */

static int smartwatch_connect(struct serio *serio, struct serio_driver *drv)
{
  struct smartwatch *smartwatch;
  struct input_dev *input_dev;
  unsigned char c = serio->id.extra;
  int err = -ENOMEM;

  smartwatch = kzalloc(sizeof(struct smartwatch), GFP_KERNEL);
  input_dev = input_allocate_device();
  if(!smartwatch || !input_dev) goto fail1;

  smartwatch->dev = input_dev;
  snprintf(smartwatch->phys, sizeof(smartwatch->phys), "%s/input0", serio->phys);
  smartwatch->type = serio->id.proto;

  input_dev->name = "SmartWatch Presenter";
  input_dev->phys = smartwatch->phys;
  input_dev->id.bustype = BUS_RS232;
  input_dev->id.vendor  = smartwatch->type;
  input_dev->id.product = c;
  input_dev->id.version = 0x0100;
  input_dev->dev.parent = &serio->dev;

  input_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REL);
  input_dev->keybit[BIT_WORD(BTN_MOUSE)] = BIT_MASK(BTN_LEFT) |
    BIT_MASK(BTN_RIGHT);
  input_dev->relbit[0] = BIT_MASK(REL_X) | BIT_MASK(REL_Y);

  set_bit(REL_WHEEL, input_dev->relbit);

  serio_set_drvdata(serio, smartwatch);

  err = serio_open(serio, drv);
  if(err) goto fail2;

  err = input_register_device(smartwatch->dev);
  if(err) goto fail3;

  return 0;

 fail3:	serio_close(serio);
 fail2:	serio_set_drvdata(serio, NULL);
 fail1:	input_free_device(input_dev);
  kfree(smartwatch);
  return err;
}

static struct serio_device_id smartwatch_serio_ids[] = {
  {
    .type	= SERIO_RS232,
    .proto	= 0x40,
    .id	= SERIO_ANY,
    .extra	= SERIO_ANY,
  },
  { 0 }
};

MODULE_DEVICE_TABLE(serio, smartwatch_serio_ids);

static struct serio_driver smartwatch_drv = {
  .driver		= {
    .name	= "SmartWatch",
  },
  .description	= DRIVER_DESC,
  .id_table	= smartwatch_serio_ids,
  .interrupt	= smartwatch_interrupt,
  .connect	= smartwatch_connect,
  .disconnect	= smartwatch_disconnect,
};

module_serio_driver(smartwatch_drv);
