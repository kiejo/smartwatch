This directory contains the software required on the notebook / desktop to use the SmartWatch Presenter.

/driver
  SmartWatch Presenter driver

/swattach
  tool used by the script swpres.sh to attach the driver to the device

Makefile
  global Makefile for driver and swattach tool

swpres.sh
  bash script to attach/detach the SmartWatch Presenter

For further details on the driver and the swattach tool refer to the Readme.txt file in the corresponding subdirectory.

##### Compilation and configuration #####

Before the first use of the SmartWatch Presenter the driver "smartwatch" and the tool "swattach" have to be compiled and the Bluetooth address of the smart phone and the Bluetooth channel used for the SmartWatch Presenter have to be entered in the bash script swpres.sh.

Run

  make
to compile the driver and the tool.

Alternatively you can run

  cd driver
  make
  cd ..
to compile the driver and

  cd swattach
  make
  cd ..
to compile the swattach tool separately.

After that start the SmartWatch Presenter App on your smart phone, ensure Bluetooth is enabled on your computer and run

  sdptool browse
to determine the required configuration. The sdptool is shipped by default with various Linux distributions.

The output of the sdptool should look like this:
Inquiring ...                      [<- may take a few seconds]
Browsing 00:21:3E:D3:2D:B4 ...     [<- here you will find the Bluetooth address of your smart phone]
[...]                              [<- here a lot of other services of your smart phone may be listed]

Service Name: SmartWatchPresenter  [<- this is the service we are interested in]
[...]                              [<- information we are not interested in]
Protocol Descriptor List:
  "L2CAP" (0x0100)
  "RFCOMM" (0x0003)
    Channel: 6                     [<- this is the channel used for the SmartWatch Presenter]

[...]                              [<- maybe even more services of your phone]

Now you have to perform the configuration. Open the "swpres.sh" file
and set the variable SWPRES_BTADDRESS to the address of your smart phone like this:
  SWPRES_BTADDRESS="00:21:3E:D3:2D:B4"

and the variable SWPRES_BTCHANNEL to the channel number used by the SmartWatch Presenter like this:
  SWPRES_BTCHANNEL=6

Both variables are located at the top of the file.

If you are already using virtual serial ports to transfer data over the RFCOMM Bluetooth protocol in your system it may be necessary to increment the variable SWPRES_COMDEV to prevent conflicts with other devices.

It is not recommended to edit the rest the bash script.

##### Usage #####

Once compiled and configured it is sufficient to run

  sudo ./swpres.sh
to establish a Bluetooth connection to the SmartWatch Presenter.

The script must not be terminated as long as the presenter is used. To detach the presenter press Ctrl-C or kill the process "swattach".

The script may be executed in background e.g. with
  sudo ./swpres.sh &
To detach the presenter when running in background kill the process "swattach", do not (!) kill the script process itself.

##### Developer documentation #####

Overview
As noted in the main Readme.txt file the user input is first transferred from the Smart Watch to a smart phone, processed by the SmartWatch Presenter App and then send to the computer that should be controlled. The transfer between smart phone and computer is performed over Bluetooth using the RFCOMM protocol. This protocol allows it to emulate a serial connection (RS-232) over Bluetooth. On the computer the rfcomm tool is used to establish the Bluetooth connection and to offer a virtual serial port (usually /dev/rfcomm0) representing one end of the emulated serial connection. To handle the arriving input the SmartWatch Presenter driver is then attached to this port using the swattach tool.

rfcomm
The rfcomm tool is executed in a coprocess because it doesn't and must not terminate as long as the Bluetooth connection to the smart phone is required. The PID of this coprocess is stored in COPROC_PID. Once executed the rfcomm tool tries to establish a connection to the smart phone. This may take a long time and even user interaction on the smart phone may be required, as the user may be asked to confirm the connection. Therefore the script periodically checks if the device file for the virtual serial port is already present. The connection was obviously successfully established if this file is present. If it is not present and the rfcomm process is still running then there is still a chance that the tool will succeed, it may only be waiting for user interaction on the smart phone, so the script just waits. If the file is not present and the rfcomm process is gone then it obviously failed to establish a connection and therefore the script can be terminated. At the end of the script the connection has to be closed and the device file has to be removed. To close the connection the rfcomm process is killed based on the value stored in COPROC_PID. As this variable is automatically cleared when the process terminates, it can not happen that a different process is accidentally killed because of a PID collision. To remove the device file the rfcomm tool is executed with the parameter "release".

SmartWatch Presenter driver
When a connection was successfully established, the SmartWatch Presenter driver (driver/smartwatch.ko) is loaded on the fly using insmod. When the user decides to disconnect the presenter the driver is unloaded. To allow this the script has to be executed with root privileges, therefore the script checks if it is running with root privileges before it tries to perform anything else.

swattach
The custom tool swattach is used to attach the SmartWatch Presenter driver to the virtual serial port. It has to run as long as the presenter is used. It is executed in blocking mode (without the parameter --daemon), allowing the script to wait until the user decides to disconnect the presenter by terminating the swattach process. Either by pressing Ctrl-C or by killing the swattach process in some other way. To ensure that the swpres.sh script is not terminated when the user presses Ctrl-C the trap command is used to temporally assign a new action to the SIGINT signal.
