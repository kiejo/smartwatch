#!/bin/bash

# use "sdptool browse" to determine the address of your smart phone
# and the channel used for the SmartWatch Presenter

# Bluetooth address of the smart phone
SWPRES_BTADDRESS="40:B0:FA:16:BE:14"

# Bluetooth channel to use to connect to the smart phone
SWPRES_BTCHANNEL=6

# device file for virtual serial port ("${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}")
# don't change the prefix (defined by rfcomm)
SWPRES_COMDEV_PREF="/dev/rfcomm"
SWPRES_COMDEV=0

# display help if unexpected arguments are present
if [[ "$#" != "0" ]]; then
  echo "" >&2
  echo "swpres.sh - SmartWatch Presenter" >&2
  echo "This script is used to attach the SmartWatch Presenter." >&2
  echo "It establishes a Bluetooth connection to the smart phone" >&2
  echo "and loads the appropriate driver to handle inputs." >&2
  echo "Once successfully attached the SmartWatch Presenter can be" >&2
  echo "detached pressing Ctrl-C or by killing the \"swattach\" process." >&2
  echo "This script has to be run as root." >&2
  echo "" >&2
  echo "Usage: swpres.sh [--help]" >&2
  echo "  --help  prints this message to standard error output and exits"
  echo "" >&2
  echo "Current configuration:" >&2
  echo "  Bluetooth address of our smart phone:       ${SWPRES_BTADDRESS}" >&2
  echo "  Bluetooth channel used for the presenter:   ${SWPRES_BTCHANNEL}" >&2
  echo "  intermediate device used for communication: \"${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}\"" >&2
  echo "The configuration can be changed by setting the" >&2
  echo "corresponding variables at the top of this bash script." >&2
  echo "For further details refer to the Readme.txt file." >&2
  echo "" >&2
  exit 1
fi

# check if we are root
if [[ "`id -u`" != "0" ]]; then
  echo "swpres: error: you have to be root to execute this script" >&2
  exit 1
fi

# try to release the device if already present (but not in use)
if [[ -a "${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}" ]]; then
  rfcomm release ${SWPRES_COMDEV}

  # terminate if the device is still present
  if [[ -a "${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}" ]]; then
    echo "swpres: error: device \"${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}\" is already in use" >&2
    exit 1
  else
    echo "swpres: warning: device \"${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}\" was released" >&2
  fi
fi

# try to establish a Bluetooth connection and attach it to a virtual serial port
coproc rfcomm connect ${SWPRES_COMDEV} "${SWPRES_BTADDRESS}" ${SWPRES_BTCHANNEL}

# wait until the virtual serial port is available
# this may take a long time and may even require user interaction on the smart phone
echo "swpres: establishing Bluetooth connection ..."
sleep 1

# check if the virtual serial port is available
until [[ -a "${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}" ]]; do
  # serial port is still not available, check if rfcomm is still running
  if ! ps -p ${COPROC_PID} > /dev/null 2>&1; then
    # rfcomm is no longer running
    echo "swpres: error: failed to establish Bluetooth connection" >&2
    exit 1
  fi

  # wait longer
  sleep 1
done

# virtual serial port is available, load our driver
if insmod driver/smartwatch.ko; then

  # attach our driver to the virtual serial port
  echo "swpres: SmartWatch Presenter successfully connected"
  echo "swpres: Press Ctrl-C or kill the process swattach to disconnect ..."
  trap "echo \"swpres: Ctrl-C received\"" SIGINT
  swattach/swattach "${SWPRES_COMDEV_PREF}${SWPRES_COMDEV}"
  trap - SIGINT
  echo "swpres: disconnecting ..."

  # unload driver
  if ! rmmod driver/smartwatch.ko; then
    echo "swpres: warning: failed to unload driver"
  fi

else
  echo "swpres: error: failed to load driver" >&2
fi

kill ${COPROC_PID} 2> /dev/null
rfcomm release ${SWPRES_COMDEV}
