swattach - SmartWatch Presenter attach tool

This tool attaches the SmartWatch Presenter driver to the virtual serial port where the input from the presenter arrives.

##### Compilation #####

Run

  make
to compile the swattach tool.

##### Usage #####

Prerequirements:
- (virtual) serial port (where the input from the SmartWatch presenter arrives)
- SmartWatch Presenter driver loaded

Run

  swattach <device>
where <device> is the device file of the virtual serial port (e.g. /dev/rfcomm0) where the input from the SmartWatch Presenter arrives. This tool will then run and ensure that the input from the presenter is processed until it is killed or an error occurs.

You can use

  swattach --daemon <device>
to run this tool as a system daemon. It will detach from the parent terminal after a successful initialization.

##### Developer documentation #####

This tool is used to tell the system that the input on a (virtual) serial port should be processed by the SmartWatch Presenter driver.

It first opens and configures the serial port with reasonable settings and flushes the input buffer of the port.

Then it uses ioctl on the serial port to attach an appropriate input line discipline to the port telling the kernel that the serial port should be handled by the module drivers/input/serio/serport.c. This module converts the tty line into a simpler "serial io port" abstraction used by input device drivers.

After that it uses ioctl on the port again to tell the system which type of device is attached to the port. This call is already handled by serport.c. The appropriate device type ID for the SmartWatch Presenter is registered by the SmartWatch Presenter driver when loaded. By matching this IDs the kernel knows that the device should be handled by the SmartWatch Presenter driver.

serport.c than receives any input received on the serial port and propagates it byte-wise to /drivers/input/serio/serio.c, an abstraction module for serial io, which in turn calls the interrupt function of the appropriate driver which is the SmartWatch Presenter driver.

Input on the serial port is propagated to the SmartWatch Presenter driver only if read operations are performed on the device file. Therefore this tool performs read operations on the device in an infinite loop after a successful initialization.
