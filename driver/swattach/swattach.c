/*
 * swattach
 *
 *  Author: TUM - LinuxC Lab - SmartWatch Group 2
 *
 *  based on inputattach by Vojtech Pavlik
 *
 */

/*
 * Input line discipline attach program
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include <errno.h>
#include <fcntl.h>
#include <linux/serio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

/* Reads a character from file discriptor with timeout. */
static int readchar(int fd, unsigned char *c, int timeout)
{
	struct timeval tv;
	fd_set set;

	tv.tv_sec = 0;
	tv.tv_usec = timeout * 1000;

	FD_ZERO(&set);
	FD_SET(fd, &set);

	if (!select(fd + 1, &set, NULL, NULL, &tv))
		return -1;

	if (read(fd, c, 1) != 1)
		return -1;

	return 0;
}

/* Performs configuration of a serial port. */
static void setline(int fd, int flags, int speed)
{
	struct termios t;

	tcgetattr(fd, &t);

	t.c_cflag = flags | CREAD | HUPCL | CLOCAL;
	t.c_iflag = IGNBRK | IGNPAR;
	t.c_oflag = 0;
	t.c_lflag = 0;
	t.c_cc[VMIN ] = 1;
	t.c_cc[VTIME] = 0;

	cfsetispeed(&t, speed);
	cfsetospeed(&t, speed);

	tcsetattr(fd, TCSANOW, &t);
}

/* Prints the usage message.
*/
static void show_help(void)
{
	puts("Usage: swattach [--daemon] <device>");
}

/* palmed wisdom from http://stackoverflow.com/questions/1674162/ */
#define RETRY_ERROR(x) (x == EAGAIN || x == EWOULDBLOCK || x == EINTR)

int main(int argc, char **argv)
{
	const unsigned long devt = 0x40;          // type of device (registered by the driver)
	int ldisc;                                // line discipline
	const char *device = NULL;                // points to the parameter from the command line specifying the device
	int daemon_mode = 0;                      // run in background
	int fd;                                   // serial port

	int i;
	unsigned char c;
	int retval;

	// parse command line parameters
	for (i = 1; i < argc; i++) {
		if (!strcasecmp(argv[i], "--help")) {
			show_help();
			return EXIT_SUCCESS;
		} else if (!strcasecmp(argv[i], "--daemon")) {
			daemon_mode = 1;
		} else if (device == NULL) {    // assume that the first unknown parameter is the device file
			device = argv[i];
		} else {                     // unexpected parameter found
			fprintf(stderr,
				"swattach: too many arguments\n");
			return EXIT_FAILURE;
		}
	}

	// check if a device file was specified
	if (device == NULL) {
		fprintf(stderr, "swattach: must specify device\n");
		return EXIT_FAILURE;
	}

	// open device file (hopefully a serial port)
	fd = open(device, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0) {
		fprintf(stderr, "swattach: '%s' - %s\n",
			device, strerror(errno));
		return EXIT_FAILURE;
	}

	// configure the serial port
	setline(fd, CS8, B1200);

	// we don't expect any bytes in the buffer, but nevertheless flush the buffer
	while (!readchar(fd, &c, 100))
		/* empty */;

	// set line discipline (we want to have a serial io port)
	ldisc = N_MOUSE;
	if (ioctl(fd, TIOCSETD, &ldisc) < 0) {
		fprintf(stderr, "swattach: can't set line discipline\n");
		return EXIT_FAILURE;
	}

	// set device type (tell the system to use our SmartWatch Presenter driver)
	if (ioctl(fd, SPIOCSTYPE, &devt) < 0) {
		fprintf(stderr, "swattach: can't set device type\n");
		return EXIT_FAILURE;
	}

	// go to background if requested by user
	retval = EXIT_SUCCESS;
	if (daemon_mode && daemon(0, 0) < 0) {
		perror("swattach");
		retval = EXIT_FAILURE;
	}

	// just perform reads
	errno = 0;
	do {
		i = read(fd, NULL, 0);
	} while (RETRY_ERROR(errno));

	ldisc = 0;
	if (errno == 0) {
		// If we've never managed to read, avoid resetting the line
		// discipline - another swattach / inputattach is probably running
		ioctl(fd, TIOCSETD, &ldisc);
	}
	close(fd);

	return retval;
}
