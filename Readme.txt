SmartWatch Presenter

This directory contains all custom software that is required to use the Smart Watch as a presenter.

/app
  SmartWatch Presenter App required on the smart phone

/driver
  software required on the notebook / desktop running the presentation

To use your Smart Watch as a presenter you have to install the SmartWatch Presenter App on your Smartphone and you need some additional software on your computer. For further details on the individual parts refere to the Readme.txt file in the corresponding subdirectory.

##### Developer documentation #####

User input is first transferred from the Smart Watch to a smart phone, processed by the SmartWatch Presenter App and then send to the computer that should be controlled. The transfer between smart phone and computer is performed over Bluetooth using the RFCOMM protocol. Further details can be found in the Readme.txt files in the subdirectories.
