##### main components of the Smartwatch Presenter App #####

BluetoothServer:
  Singleton class that controls pairing and communication with Laptop/PC (used by ControlSmartWatch and SmartwatchPresenterPreferenceActivity)

ControlSmartWatch:
  Controls communication with smartwatch (touch events, screen content)

SmartwatchPresenterPreferenceActivity:
  UI that gets displayed through Sony's SmartConnect App
  
SmartwatchPresenterRegistrationInformation, SmartwatchPresenterExtensionService, ExtensionReceiver:
  Handle the registration and integration of the Smartwatch Presenter App inside Sony's SmartConnect App
  

For more detailed documentation refer to the comments in the source code
