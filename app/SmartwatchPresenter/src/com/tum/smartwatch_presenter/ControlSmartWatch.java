/*
Copyright (c) 2011, Sony Ericsson Mobile Communications AB
Copyright (c) 2011-2013, Sony Mobile Communications AB

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Ericsson Mobile Communications AB / Sony Mobile
 Communications AB nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.tum.smartwatch_presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;

import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.control.ControlTouchEvent;
import com.example.sonymobile.smartextension.backwardscompatiblecontrol.R;

// signals we send to the server
enum SmartwatchSignal {
	Left,
	Right
}

/**
 * Controls the interaction (touch, screen) with the Smartwatch and sends signals to server.
 */
class ControlSmartWatch extends ControlExtension {
	public static final int SCREEN_WIDTH = 128;
	public static final int SCREEN_HEIGHT = 128;
	
	private Rect leftSide = new Rect(0, 0, SCREEN_WIDTH / 2, SCREEN_HEIGHT);
	private Rect rightSide = new Rect(SCREEN_WIDTH / 2, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	Bitmap arrows_inactive;
	Bitmap arrows_left;
	Bitmap arrows_right;
	
    /**
     * Create smartwatch control.
     *
     * @param hostAppPackageName Package name of host application.
     * @param context The context.
     * @param handler The handler to use
     */
    ControlSmartWatch(final String hostAppPackageName, final Context context, Handler handler) {
        super(context, hostAppPackageName);
        if (handler == null) {
            throw new IllegalArgumentException("handler == null");
        }
        
        this.arrows_inactive = BitmapFactory.decodeResource(context.getResources(), R.drawable.arrows_inactive);
        this.arrows_left = BitmapFactory.decodeResource(context.getResources(), R.drawable.arrows_left_active);
        this.arrows_right = BitmapFactory.decodeResource(context.getResources(), R.drawable.arrows_right_active);
    }

    @Override
    public void onDestroy() {
        Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "ControlSmartWatch onDestroy");
    };

    @Override
    public void onStart() {
        // Nothing to do. Animation is handled in onResume.
    }

    @Override
    public void onStop() {
        // Nothing to do. Animation is handled in onPause.
    }

    @Override
    public void onResume() {
        Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "resume");

        showBitmap(arrows_inactive);
    }
    
    @Override
    public void onPause() {
        Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "pause");
    }


    @Override
    public void onTouch(final ControlTouchEvent event) {
    	/*
    	 *  if screen is not touched, show normal (inactive) arrows
    	 *  if screen is touched, show activated left/right arrow
    	 */
    	
    	// screen just got released => send signal to server
        if (event.getAction() == Control.Intents.TOUCH_ACTION_RELEASE) {
        	Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "touched");
        	showBitmap(arrows_inactive);
        	
        	if (leftSide.contains(event.getX(), event.getY())) {
        		BluetoothServer.getSharedInstance().sendTouchEvent(SmartwatchSignal.Left);
        	}
        	else if (rightSide.contains(event.getX(), event.getY())) {
        		BluetoothServer.getSharedInstance().sendTouchEvent(SmartwatchSignal.Right);
        	}
        }
        // screen is being touched => update screen
        else if (event.getAction() == Control.Intents.TOUCH_ACTION_PRESS ||
        		 event.getAction() == Control.Intents.TOUCH_ACTION_LONGPRESS) {
        	
        	Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "press, " + event.getX() + " " + event.getY());
        	if (leftSide.contains(event.getX(), event.getY())) {
        		Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "left");
        		showBitmap(arrows_left);
        	}
        	else if (rightSide.contains(event.getX(), event.getY())) {
        		Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "right");
        		showBitmap(arrows_right);
        	}
        }
        else {
        	Log.d(SmartwatchPresenterExtensionService.LOG_TAG, "other touch");
        }
    }
}
