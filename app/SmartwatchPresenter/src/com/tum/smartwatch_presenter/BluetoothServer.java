package com.tum.smartwatch_presenter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

public class BluetoothServer {
	
	// we need a shared server instance so that we can access it after it has been started in the settings dialogue
	private static BluetoothServer serverInstance = null; // shared server instance
	public static BluetoothServer getSharedInstance(){
		if (serverInstance == null) {
			serverInstance = new BluetoothServer();
		}
		
		return serverInstance;
	}
	
    private String defaultUUID = "00001101-0000-1000-8000-00805F9B34FB";
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); // the device's bluetooth adapter
    private final int REQ = 111; //used to identify the bluetooth request dialogue
    
    private AcceptThread server;
    
    // we need a reference to the parent activity to show the request bluetooth discoverability dialogue
    private Activity parentActivity;
    public void setParentActivity(Activity activity) {
    	this.parentActivity = activity;
    }
    
    private BluetoothServer() { } // only instantiated through the getSharedInstance method
    
    /**
     * Launches Discoverable Bluetooth Intent.
     */
    public void requestBTDiscoverable() {
        Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        i.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);

        this.parentActivity.startActivityForResult(i, REQ);

        Log.i("as", "Bluetooth discoverability enabled");
    }
    
    public void startServer() {
    	// make sure the device is discoverable before we start the server
        if (bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
            requestBTDiscoverable();

        this.server = new AcceptThread();
        this.server.start();
    }
    
    public void sendTouchEvent(SmartwatchSignal signal){
    	switch (signal) {
    		case Left:
    			this.server.sendMessage(new byte[] { 0x01 }); // server translates this into mouse wheel scroll up
    			break;
    		case Right:
    			this.server.sendMessage(new byte[] { 0x02 }); // server translates this into mouse wheel scroll down
    			break;
    	}
    }

    // Waits for incoming connection, accepts it and handles write requests
    class AcceptThread extends Thread {
        private final String ACCEPT_TAG = "SmartWatchPresenter"; // this TAG will appear in the discover bluetooth log
        
        private BluetoothServerSocket serverSocket;
        private BluetoothSocket acceptedSocket;
        
        public void run() {        	
        	try {
        		// start listening for incoming connections
                this.serverSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(ACCEPT_TAG, UUID.fromString(defaultUUID));
                Log.i(ACCEPT_TAG, "Listening for a connection...");
                
                // accept incoming connection
                this.acceptedSocket = serverSocket.accept();
                Log.i(ACCEPT_TAG, "Connected to " + this.acceptedSocket.getRemoteDevice().getName());
                
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        // close open connections
        public void cancel() {
        	try {
            	this.serverSocket.close();
            	this.acceptedSocket.close();
            	Log.i(ACCEPT_TAG, "Closed connection");
            } catch (IOException e) { }
        }
        
        public void sendMessage(byte[] message) {
        	// send message if connection is open
        	if (this.acceptedSocket != null) {
        		try {
        			Log.i(ACCEPT_TAG, "Sending message " + message);
            		DataOutputStream stream = new DataOutputStream(this.acceptedSocket.getOutputStream());
            		stream.write(message);
            		stream.flush();
            	}
            	catch(IOException e) {
            		Log.e(ACCEPT_TAG, "Error obtaining OutputStream from socket");
                    e.printStackTrace();
            	}
        	}	
        }
    }
}
