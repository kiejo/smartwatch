This directory contains the Smartwatch Presenter App.

/SmartExtensionAPI
  library provided by Sony (needed to integrate third party applications into Sony's SmartConnect App)

/SmartExtensionUtils
  library provided by Sony (needed to integrate third party applications into Sony's SmartConnect App)

/SmartwatchPresenter
  source of the app


##### Dependencies #####

- Android SDK (http://developer.android.com/sdk/index.html)
- Sony Add-on SDK 3.0 (install through Android SDK Manager: http://developer.sonymobile.com/knowledge-base/sony-add-on-sdk/install-the-sony-add-on-sdk/)
- SmartExtensionAPI (needs to be added to the Java build path in the SmartwatchPresenter project)
- SmartwatchPresenter (needs to be added to the Java build path in the SmartwatchPresenter project)

##### Development Environment / Build process #####

The SmartwatchPresenter project uses the standard Android build system (Ant). Detailed documentation on how to set up an Android development environment can be found here: http://developer.android.com/tools/workflow/index.html

More information on Android's build system can be found here: http://developer.android.com/tools/building/index.html

##### Install #####

For the Smartwatch Presenter App to work, Sony's SmartConnect App has to be installed on the device:
https://play.google.com/store/apps/details?id=com.sonyericsson.extras.liveware

The Presenter App can be installed on the device using the standard Android build process.

##### Usage #####

Once the Smartwatch Presenter App and Sony's SmartConnect App are installed, the Presenter App can be found through Sony's SmartConnect App following these Screens:
SmartWatch -> SmartWatch Edit settings -> Applications -> SmartwatchPresenter

When inside the SmartwatchPresenter screen there is an option to enable/disable the App and a "SmartwatchPresenter settings" option.
To establish a bluetooth connection with a computer, click on the "Start Server" button inside the settings screen and follow the on-screen instructions. After the two devices have been paired you can use the Smartwatch to control presentations running on the computer by clicking on the left or right arrow displayed on the Smartwatch screen.

